package microplate

import (
	"bytes"
	"crypto/sha256"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

func createDirectory(directoryPath string) {
	if err := os.Mkdir(temporaryDirectory, os.ModePerm); err != nil {
		log.Fatal(err)
	}
}

func removeDirectory(directoryPath string) {
	if err := os.RemoveAll(directoryPath); err != nil {
		log.Fatal(err)
	}
}

func copyFileToMainProject(filePath string) {
	data, err := ioutil.ReadFile(filePath)
	checkError(err)

	prefixToRemove := temporaryDirectory + "/"
	mainProjectFilePath := filePath[len(prefixToRemove):]

	if _, err := os.Stat(mainProjectFilePath); os.IsNotExist(err) {
		os.MkdirAll(filepath.Dir(mainProjectFilePath), 0700)
	}

	err = ioutil.WriteFile(mainProjectFilePath, data, 0644)
	checkError(err)

	fmt.Println("- " + mainProjectFilePath)
}

func getMainFolderPathFromTemporary(filePath string) string {
	prefixToRemove := temporaryDirectory + "/"
	return filePath[len(prefixToRemove):]
}

func copyFilesFromTemporaryDirectoryToMainProject() {
	fmt.Println("Copying files to main project")
	config := readConfig()
	filepath.Walk(temporaryDirectory, func(filepath string, info os.FileInfo, err error) error {
		if err == nil {
			if !info.IsDir() {
				if !stringInSlice(getMainFolderPathFromTemporary(filepath), config.Override) {
					copyFileToMainProject(filepath)
				}
			}
		}

		return nil
	})
	fmt.Println("Copying files Finished")
}

func getFileHash(filePath string) []byte {
	f, err := os.Open(filePath)
	checkError(err)
	defer f.Close()

	h := sha256.New()
	if _, err := io.Copy(h, f); err != nil {
		log.Fatal(err)
	}

	return h.Sum(nil)
}

func getFilesChanged() []string {
	filesChanged := []string{}
	config := readConfig()

	filepath.Walk(temporaryDirectory, func(filePath string, info os.FileInfo, err error) error {
		if err == nil {
			if !info.IsDir() {
				mainProjectFilePath := getMainFolderPathFromTemporary(filePath)

				for _, filePathToIgnore := range config.Override {
					if filePathToIgnore == mainProjectFilePath {
						return nil
					}
				}

				_, err := os.Open(mainProjectFilePath)
				if err != nil {
					return nil
				}

				temporaryFileHash := getFileHash(filePath)
				mainProjectFileHash := getFileHash(mainProjectFilePath)

				if bytes.Compare(temporaryFileHash, mainProjectFileHash) != 0 {
					filesChanged = append(filesChanged, mainProjectFilePath)
				}
			}
		}
		return nil
	})

	return filesChanged
}

func checkIfAnyFilesChanged() bool {
	filesChanged := getFilesChanged()
	return len(filesChanged) != 0
}

func printFilesChanged() {
	filesChanged := getFilesChanged()

	if len(filesChanged) != 0 {
		fmt.Println("\nFiles changed:")
		for _, element := range filesChanged {
			fmt.Println("- " + element)
		}
		fmt.Println("")
	}
}

func deleteGitFolderInTemporaryDirectory() {
	removeDirectory(temporaryDirectory + "/.git")
}
