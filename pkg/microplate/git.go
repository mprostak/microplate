package microplate

import (
	"fmt"
	"os"

	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
)

func copyBoilerplateProjectToTemporaryDirectory(config Config) {
	if config.Project.Hash != "" {
		copyBoilerplateProjectToTemporaryDirectoryUsingHash(config)
	} else {
		copyBoilerplateProjectToTemporaryDirectoryUsingBranch(config)
	}
}

func copyBoilerplateProjectToTemporaryDirectoryUsingBranch(config Config) {
	branchName := config.Project.Version
	branch := fmt.Sprintf("refs/heads/%s", branchName)

	_, err := git.PlainClone(temporaryDirectory, false, &git.CloneOptions{
		URL:           config.Project.Url,
		Progress:      os.Stdout,
		SingleBranch:  true,
		ReferenceName: plumbing.ReferenceName(branch),
	})
	checkError(err)
}

func copyBoilerplateProjectToTemporaryDirectoryUsingHash(config Config) {
	repository, err := git.PlainClone(temporaryDirectory, false, &git.CloneOptions{
		URL:      config.Project.Url,
		Progress: os.Stdout,
	})
	checkError(err)

	_, err = repository.Head()
	checkError(err)

	workTree, err := repository.Worktree()
	checkError(err)

	err = workTree.Checkout(&git.CheckoutOptions{
		Hash: plumbing.NewHash(config.Project.Hash),
	})
	checkError(err)
}
