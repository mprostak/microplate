package microplate

import (
	"log"
	"os"

	"github.com/manifoldco/promptui"
)

func checkError(err error) {
	if err != nil {
		removeDirectory(temporaryDirectory)
		log.Fatal(err)
	}
}

func createSimpleFile(filename string, content string) {
	f, err := os.Create(filename)
	checkError(err)

	_, err = f.WriteString(content)
	if err != nil {
		log.Fatal(err)
		f.Close()
		return
	}

	err = f.Close()
	checkError(err)
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func yesNo() bool {
	prompt := promptui.Select{
		Label: "Select[Yes/No]",
		Items: []string{"No", "Yes"},
	}
	_, result, err := prompt.Run()
	if err != nil {
		log.Fatalf("Prompt failed %v\n", err)
	}
	return result == "Yes"
}
