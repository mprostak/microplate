package microplate

import (
	"fmt"
	"log"
)

func Pull() {
	config := readConfig()
	fmt.Println("Getting boiler plate from " + config.Project.Url)
	removeDirectory(temporaryDirectory)
	createDirectory(temporaryDirectory)
	copyBoilerplateProjectToTemporaryDirectory(config)
	deleteGitFolderInTemporaryDirectory()
	filesChanged := checkIfAnyFilesChanged()
	if filesChanged {
		printFilesChanged()
		fmt.Println("These files have changed, do you want to override these files?")
		shouldOverride := yesNo()
		fmt.Println(shouldOverride)

		if !shouldOverride {
			removeDirectory(temporaryDirectory)
			log.Fatal("Error while copying files")
		}
	}
	copyFilesFromTemporaryDirectoryToMainProject()
	removeDirectory(temporaryDirectory)
}
