package microplate

import (
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Version  string   `yaml:"version"`
	Project  Project  `yaml:"project"`
	Override []string `yaml:"override"`
}

type Project struct {
	Url     string `yaml:"url"`
	Version string `yaml:"version"`
	Hash    string `yaml:"hash"`
}

var configPath = "microplate.yml"
var temporaryDirectory = ".microplate-tmp"

func generateConfig() {
	project := Project{
		Url:     "https://github.com/mprostakk/integral.git",
		Version: "master",
	}

	config := Config{
		Version:  "1",
		Project:  project,
		Override: []string{"README.md"},
	}

	configYamlData, _ := yaml.Marshal(&config)
	createSimpleFile(configPath, string(configYamlData))
}

func readConfig() Config {
	var config Config
	yamlFile, err := os.ReadFile(configPath)
	checkError(err)
	yaml.Unmarshal(yamlFile, &config)
	return config
}
