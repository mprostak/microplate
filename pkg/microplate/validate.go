package microplate

import "log"

func Validate() {
	config := readConfig()
	removeDirectory(temporaryDirectory)
	createDirectory(temporaryDirectory)
	copyBoilerplateProjectToTemporaryDirectory(config)
	deleteGitFolderInTemporaryDirectory()

	filesChanged := checkIfAnyFilesChanged()
	if filesChanged {
		printFilesChanged()
		removeDirectory(temporaryDirectory)
		log.Fatal("Error while validating")
	}
	log.Println("Validation complete")
}
