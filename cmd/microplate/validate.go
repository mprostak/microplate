package microplate

import (
	"github.com/mprostakk/microplate/pkg/microplate"
	"github.com/spf13/cobra"
)

var validateCmd = &cobra.Command{
	Use:     "validate",
	Aliases: []string{"v"},
	Short:   "Validate if the newest boilerplate files are used",
	Args:    cobra.ExactArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		microplate.Validate()
	},
}

func init() {
	rootCmd.AddCommand(validateCmd)
}
