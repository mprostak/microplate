package microplate

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var version = "0.0.1"

var rootCmd = &cobra.Command{
	Use:   "microplate",
	Short: "microplate - a CLI that allows to manage microservice boilerplates",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Hello from microplate")
	},
	Version: version,
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintf(os.Stderr, "Whoops. There was an error while executing a microplate command '%s'", err)
		os.Exit(1)
	}
}
