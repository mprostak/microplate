package microplate

import (
	"github.com/mprostakk/microplate/pkg/microplate"
	"github.com/spf13/cobra"
)

var pullCmd = &cobra.Command{
	Use:     "pull",
	Aliases: []string{"p"},
	Short:   "Copies files from boilerplate into current directory",
	Args:    cobra.ExactArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		microplate.Pull()
	},
}

func init() {
	rootCmd.AddCommand(pullCmd)
}
