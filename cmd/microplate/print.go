package microplate

import (
	"github.com/mprostakk/microplate/pkg/microplate"
	"github.com/spf13/cobra"
)

var printCmd = &cobra.Command{
	Use:     "print",
	Aliases: []string{"p"},
	Short:   "Print hello world",
	Args:    cobra.ExactArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		microplate.Print()
	},
}

func init() {
	rootCmd.AddCommand(printCmd)
}
