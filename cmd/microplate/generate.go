package microplate

import (
	"github.com/mprostakk/microplate/pkg/microplate"
	"github.com/spf13/cobra"
)

var generateCmd = &cobra.Command{
	Use:     "generate",
	Aliases: []string{"g"},
	Short:   "Generate example config",
	Args:    cobra.ExactArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		microplate.Generate()
	},
}

func init() {
	rootCmd.AddCommand(generateCmd)
}
