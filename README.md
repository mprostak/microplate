# Microplate

This project allows to quickly create microservices using custom defined boilerplates.

Microplate allows to quickly create new services, based on the same pattern.
The versioning system allows to quickly update and bump to a newest boilerplate version.


The architecture pattern microplate is based on is Microservice chassis described here https://microservices.io/patterns/microservice-chassis.html

Our main goal is for you to be as DRY as possible with developing your microservices.

## How can we define microplate?

It combines the features of a boilerplate and a library. It does not replace normal libraries used in your project!

![Image](docs/simple.drawio.svg "simple")


## Usage architecture

![Image](docs/architecture.drawio.svg "example")


## Example

Code that might be duplicated (example python fastapi app):

Application logic:
- main app setup creation
- app logger in setup
- middlewares in setup
- database connection
- app structure with healthcheck endpoint
- testing structure
- example setup tests

Linter:
- pre-commit files
- isort files / black / flake files
- pyproject.yml

CI:
- ci template
- requirements.base.txt
- requirements.ci.txt

Other:
- Dockerfile
- docker-compose
- Makefile commands for precommit, testing, etc.


## CLI

### Microservice commands

`microplate pull`

`microplate validate`


## Config structure

Please run `microplate generate` or 
create a `microplate.yml` file in your project directory with this structure:
```
version: "1"
project:
    url: "https://github.com/mprostakk/integral.git"
    version: main
    hash: 1fdeeff600bb39f6197519e8aedd81fc7d88f011
override:
    - README.md
```