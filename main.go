package main

import (
	"github.com/mprostakk/microplate/cmd/microplate"
)

func main() {
	microplate.Execute()
}
